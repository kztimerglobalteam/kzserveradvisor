# KZServerAdvisor
#### A SourceMod plugin to help you verify your server configuration for KZ "Global" environment.

---


## **Installation**
- Download the plugin from the [downloads tab](https://bitbucket.org/kztimerglobalteam/kzserveradvisor/downloads/) or [click here for the latest version](https://bitbucket.org/kztimerglobalteam/kzserveradvisor/downloads/KZServerAdvisor-latest.smx).
- Drop the downloaded smx file into the `/csgo/addons/sourcemod/plugins` directory.

---

## **Commands**
- `sm_kzserveradvisor_gokz` - Tests your server against [GOKZ](https://bitbucket.org/kztimerglobalteam/gokz/) global environment.
- `sm_kzserveradvisor_kztimer` - Tests your server against [KZTimer](https://bitbucket.org/kztimerglobalteam/kztimerglobal/) global environment.

---

## **Sample output**
```
Version: 1.0.0
Timestamp: 1583442296 (03/05/20 23:04:56)

Ran by: Console
Server Hostname: "My Amazing Server"
Server IP & Port: 127.0.0.1:27015

Plugin list:
  Name                               Version                          File
  - Admin File Reader                1.10.0.6453                      admin-flatfile.smx
  - Admin Help                       1.10.0.6453                      adminhelp.smx
  - Admin Menu                       1.10.0.6453                      adminmenu.smx
  - Anti-Flood                       1.10.0.6453                      antiflood.smx
  - Basic Ban Commands               1.10.0.6453                      basebans.smx
  - Basic Chat                       1.10.0.6453                      basechat.smx
  - Basic Comm Control               1.10.0.6453                      basecomm.smx
  - Basic Commands                   1.10.0.6453                      basecommands.smx
  - Basic Info Triggers              1.10.0.6453                      basetriggers.smx
  - Basic Votes                      1.10.0.6453                      basevotes.smx
  - Client Preferences               1.10.0.6453                      clientprefs.smx
  - Fun Commands                     1.10.0.6453                      funcommands.smx
  - Fun Votes                        1.10.0.6453                      funvotes.smx
  - Nextmap                          1.10.0.6453                      nextmap.smx
  - Player Commands                  1.10.0.6453                      playercommands.smx
  - Reserved Slots                   1.10.0.6453                      reservedslots.smx
  - Sound Commands                   1.10.0.6453                      sounds.smx

Test results:

> GlobalAPI-Core
[STATE]		FAIL
[ERROR]		The plugin does not appear to be installed.
[NOTE ]		Make sure the plugin (smx) exists in the plugins folder.

> Server Tickrate
[STATE]		PASS

> Server Password
[STATE]		OKAY
[NOTE ]		This server seems to be password protected, make sure the Global Team has the password always!

> GOKZ-Global
[STATE]		FAIL
[ERROR]		The plugin does not appear to be installed.
[NOTE ]		Make sure the plugin (smx) exists in the plugins folder.
```