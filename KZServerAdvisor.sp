#pragma semicolon 1
#pragma newdecls required
#pragma dynamic 131072

#include "KZServerAdvisor/utils.sp"
#include "KZServerAdvisor/tests.sp"

ArrayList g_GOKZ = null;
ArrayList g_KZTimer = null;

ConVar gCV_HostIp = null;
ConVar gCV_HostPort = null;
ConVar gCV_HostName = null;

#define PLUGIN_VERSION "1.2.0"

public Plugin myinfo =
{
    name = "KZ Server Advisor",
    author = "The KZ Global Team",
    description = "",
    version = PLUGIN_VERSION,
    url = "forum.gokz.org"
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
    RegAdminCmd("sm_kzserveradvisor_gokz", Command_TestGOKZ, ADMFLAG_RCON);
    RegAdminCmd("sm_kzserveradvisor_kztimer", Command_TestKZTimer, ADMFLAG_RCON);
}

public void OnPluginStart()
{
    g_GOKZ = new ArrayList(sizeof(Test));
    g_KZTimer = new ArrayList(sizeof(Test));

    gCV_HostIp = FindConVar("hostip");
    gCV_HostPort = FindConVar("hostport");
    gCV_HostName = FindConVar("hostname");

    RegisterTests();
}

public Action Command_TestGOKZ(int client, int args)
{
    DoTests(g_GOKZ, client);
    return Plugin_Handled;
}

public Action Command_TestKZTimer(int client, int args)
{
    DoTests(g_KZTimer, client);
    return Plugin_Handled;
}

void RegisterTest(ArrayList tests, Function handler, char name[sizeof(Test::Name)])
{
    Test test;
    test.Name = name;
    test.Handler = handler;

    tests.PushArray(test);
}

void RegisterTests()
{
    RegisterTest(g_GOKZ, Test_ServerTickrate, "Server tickrate");
    RegisterTest(g_KZTimer, Test_ServerTickrate, "Server tickrate");

    RegisterTest(g_GOKZ, Test_ServerPassword, "Server password");
    RegisterTest(g_KZTimer, Test_ServerPassword, "Server password");

    RegisterTest(g_GOKZ, Test_NoBotsParameter, "-nobots parameter");
    RegisterTest(g_KZTimer, Test_NoBotsParameter, "-nobots parameter");

    RegisterTest(g_GOKZ, Test_BadPlugins, "Bad plugins");
    RegisterTest(g_KZTimer, Test_BadPlugins, "Bad plugins");

    // GOKZ
    RegisterTest(g_GOKZ, Test_GlobalAPI, "GlobalAPI");
    RegisterTest(g_GOKZ, Test_GOKZGlobal, "GOKZ-Global");

    // KZTimer
    RegisterTest(g_KZTimer, Test_KZTimerAPI, "KZTimer-API");
    RegisterTest(g_KZTimer, Test_GlobalAPICore, "GlobalAPI-Core");
    RegisterTest(g_KZTimer, Test_GlobalAPIJumpstats, "GlobalAPI-Jumpstats");
}

void DoTests(ArrayList tests, int client)
{
    PrintHeader(client);
    PrintPlugins();

    LogMessage("Test results:");
    for (int i = 0; i < tests.Length; i++)
    {
        Test test;
        tests.GetArray(i, test);

        TestResult r = DoTest(test);

        LogMessage("> %s", test.Name);
        LogMessage("    [STATE]    %s", ResultPhrases[r]);

        if (r == TestResult_Fail)
        {
            LogMessage("    [ERROR]    %s", test.Error);
        }

        if (test.Message[0] != '\0')
        {
            LogMessage("    [NOTE ]    %s", test.Message);
        }
    }

    ReplyToCommand(client, "See the sourcemod logs for the output!");
}

void PrintHeader(int client)
{
    char dateTime[32];
    FormatTime(dateTime, sizeof(dateTime), "%x %X", GetTime());

    char hostIpPort[64];
    GetServerIPPort(hostIpPort, sizeof(hostIpPort));

    char hostName[128];
    gCV_HostName.GetString(hostName, sizeof(hostName));

    char steamId64[32] = "N/A";
    if (client > 0 && IsClientInGame(client))
    {
        GetClientAuthId(client, AuthId_SteamID64, steamId64, sizeof(steamId64));
    }

    LogMessage("Version: %s", PLUGIN_VERSION);
    LogMessage("Timestamp: %d (%s)", GetTime(), dateTime);

    LogMessage("Ran by: %N (%s)", client, steamId64);
    LogMessage("Server Hostname: \"%s\"", hostName);
    LogMessage("Server IP & Port: \"%s\"", hostIpPort);
}

void PrintPlugins()
{
    LogMessage("Plugin list:");
    LogMessage("  %34s %32s %s", "Name", "Version", "File");

    Handle iter = GetPluginIterator();
    while (MorePlugins(iter))
    {
        Handle plugin = ReadPlugin(iter);
        if (plugin == GetMyHandle())
        {
            continue;
        }

        char fileName[PLATFORM_MAX_PATH];
        GetPluginFilename(plugin, fileName, sizeof(fileName));

        char name[34] = "Unknown";
        GetPluginInfo(plugin, PlInfo_Name, name, sizeof(name));

        // maxlen of 32
        if (strlen(name) >= 32)
        {
            // Terminate 3 chars before to fit dots
            name[(32 - 3)] = '\0';
            StrCat(name, sizeof(name), "...");
        }

        char version[32] = "Unknown";
        GetPluginInfo(plugin, PlInfo_Version, version, sizeof(version));

        LogMessage("  - %32s %32s %s", name, version, fileName);
    }

    delete iter;
}

void GetServerIPPort(char[] buffer, int maxlength)
{
    int port = gCV_HostPort.IntValue;
    int longIp = gCV_HostIp.IntValue;

    int octets[4];
    octets[0] = (longIp >> 24)  & 0x000000FF;
    octets[1] = (longIp >> 16)  & 0x000000FF;
    octets[2] = (longIp >> 8)   & 0x000000FF;
    octets[3] = (longIp)        & 0x000000FF;

    Format(buffer, maxlength, "%d.%d.%d.%d:%d",
                                octets[0],
                                octets[1],
                                octets[2],
                                octets[3],
                                port
    );
}
