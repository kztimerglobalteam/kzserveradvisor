bool FileExistsAndIsNotEmpty(char[] filePath)
{
    if (!FileExists(filePath))
    {
        return false;
    }

    if (FileSize(filePath) <= 0)
    {
        return false;
    }

    return true;
}

bool PluginFileExistsPartial(char[] plugin)
{
    char path[PLATFORM_MAX_PATH];
    BuildPath(Path_SM, path, sizeof(path), "plugins");

    DirectoryListing dirList = OpenDirectory(path);
    if (dirList == null)
    {
        return false;
    }

    char buffer[PLATFORM_MAX_PATH];
    FileType type = FileType_Unknown;

    while (dirList.GetNext(buffer, sizeof(buffer), type))
    {
        if (type != FileType_File)
        {
            continue;
        }

        // This assumes "smx" file extension
        if (StrContains(buffer, plugin, false) == 0)
        {
            delete dirList;
            return true;
        }
    }

    delete dirList;
    return false;
}
