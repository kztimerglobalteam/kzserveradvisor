// ===== [ IMPLEMENTATION ] =====

enum TestResult
{
    TestResult_Fail,
    TestResult_Okay,
    TestResult_Pass,
    TestResult_COUNT
};

char ResultPhrases[TestResult_COUNT][] =
{
    "FAIL",
    "OKAY",
    "PASS"
};

enum struct Test
{
    char Name[128];
    char Error[1024];
    char Message[1024];

    Function Handler;
}

TestResult DoTest(Test test)
{
    test.Error = "";
    test.Message = "";

    TestResult result = TestResult_Fail;
    Call_StartFunction(null, test.Handler);
    Call_PushArrayEx(test, sizeof(test), SP_PARAMFLAG_BYREF);
    Call_Finish(result);
    return result;
}

// ===== [ TESTS ] =====

#include "KZServerAdvisor/tests/ServerTickrate.sp"
#include "KZServerAdvisor/tests/ServerPassword.sp"
#include "KZServerAdvisor/tests/NoBotsParameter.sp"
#include "KZServerAdvisor/tests/BadPlugins.sp"

// GOKZ
#include "KZServerAdvisor/tests/gokz/GlobalAPI.sp"
#include "KZServerAdvisor/tests/gokz/GOKZGlobal.sp"

// KZTimer
#include "KZServerAdvisor/tests/kztimer/KZTimerAPI.sp"
#include "KZServerAdvisor/tests/kztimer/GlobalAPICore.sp"
#include "KZServerAdvisor/tests/kztimer/GlobalAPIJumpstats.sp"
