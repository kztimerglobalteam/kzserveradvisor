public TestResult Test_ServerTickrate(Test test)
{
    bool isBadTickrate = FloatAbs(1.0 / GetTickInterval() - 128.0) > 0.000001;
    if (isBadTickrate)
    {
        test.Error = "Unsupported tickrate";
        test.Message = "Currently for compability reasons, only 128 tick servers are supported";
        return TestResult_Fail;
    }

    return TestResult_Pass;
}
