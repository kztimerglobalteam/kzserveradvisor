static char BadPlugins[][] =
{
    "funcommands",
    "playercommands"
};

public TestResult Test_BadPlugins(Test test)
{
    bool hasBadPlugins = false;
    for (int i = 0; i < sizeof(BadPlugins); i++)
    {
        if (PluginFileExistsPartial(BadPlugins[i]))
        {
            if (!hasBadPlugins)
            {
                hasBadPlugins = true;
                Format(test.Message, sizeof(test.Message), "Remove the following plugins: %s", BadPlugins[i]);
            }
            else
            {
                Format(test.Message, sizeof(test.Message), "%s, %s", test.Message, BadPlugins[i]);
            }
        }
    }

    if (hasBadPlugins)
    {
        test.Error = "The server is running bad plugins, see the notes for more information";
        return TestResult_Fail;
    }

    return TestResult_Pass;
}
