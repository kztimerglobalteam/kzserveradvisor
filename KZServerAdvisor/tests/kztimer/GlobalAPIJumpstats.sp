public TestResult Test_GlobalAPIJumpstats(Test test)
{
    if (!LibraryExists("GlobalAPI-Jumpstats"))
    {
        if (!PluginFileExistsPartial("GlobalAPI-Jumpstats"))
        {
            test.Error = "The plugin does not appear to be installed";
            test.Message = "Make sure the plugin (smx) exists in the plugins folder";
            return TestResult_Fail;
        }

        test.Error = "The plugin seems to not be operational";
        test.Message = "This is most likely due to GlobalAPI-Core not loading, see your error logs";
        return TestResult_Fail;
    }

    return TestResult_Pass;
}
