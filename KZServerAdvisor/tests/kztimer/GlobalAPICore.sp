public TestResult Test_GlobalAPICore(Test test)
{
    if (!LibraryExists("GlobalAPI-Core"))
    {
        if (!PluginFileExistsPartial("GlobalAPI-Core"))
        {
            test.Error = "The plugin does not appear to be installed";
            test.Message = "Make sure the plugin (smx) exists in the plugins folder";
            return TestResult_Fail;
        }

        test.Error = "The plugin seems to not be operational";
        test.Message = "Make sure you have the SteamWorks extension installed, see your error logs for more";
        return TestResult_Fail;
    }

    if (!FileExistsAndIsNotEmpty("cfg/sourcemod/globalrecords.cfg"))
    {
        test.Error = "cfg/sourcemod/globalrecords.cfg file does not exist or is empty";
        test.Message = "Make sure to put your API key (even if unapproved) inside the file";
        return TestResult_Fail;
    }

    return TestResult_Pass;
}
