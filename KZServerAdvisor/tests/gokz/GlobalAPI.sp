public TestResult Test_GlobalAPI(Test test)
{
    if (!LibraryExists("GlobalAPI"))
    {
        if (!PluginFileExistsPartial("GlobalAPI"))
        {
            test.Error = "The plugin does not appear to be installed";
            test.Message = "Make sure the plugin (smx) exists in the plugins folder";
            return TestResult_Fail;
        }

        test.Error = "The plugin seems to not be operational";
        test.Message = "Make sure you have the SteamWorks extension installed, see your error logs for more";
        return TestResult_Fail;
    }

    if (!FileExistsAndIsNotEmpty("cfg/sourcemod/globalapi-key.cfg"))
    {
        char altPath[PLATFORM_MAX_PATH];
        BuildPath(Path_SM, altPath, sizeof(altPath), "configs/globalapi-key.cfg");

        if (!FileExistsAndIsNotEmpty(altPath))
        {
            test.Error = "cfg/sourcemod/globalapi-key.cfg file does not exist or is empty";
            test.Message = "Make sure to put your API key (even if unapproved) inside the file";
            return TestResult_Fail;
        }
    }

    return TestResult_Pass;
}
