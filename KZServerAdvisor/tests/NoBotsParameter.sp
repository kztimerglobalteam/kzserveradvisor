public TestResult Test_NoBotsParameter(Test test)
{
    bool hasNoBotParam = FindCommandLineParam("-nobots");
    if (hasNoBotParam)
    {
        test.Error = "-nobots parameter is specified, replay bots cannot function with it";
        test.Message = "Remove the \"-nobots\" parameter from your srcds commandline arguments";
        return TestResult_Fail;
    }

    return TestResult_Pass;
}
