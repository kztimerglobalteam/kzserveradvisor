public TestResult Test_ServerPassword(Test test)
{
    ConVar sv_password = FindConVar("sv_password");
    if (sv_password != null)
    {
        char password[256];
        sv_password.GetString(password, sizeof(password));

        if (password[0] != '\0')
        {
            test.Message = "This server seems to be password protected, make sure the Global Team has the password always!";
            return TestResult_Okay;
        }
    }

    return TestResult_Pass;
}
